![Logo HEIAFR](logo.png)

# PV de séance du 15.07, 10h30
Membres : Frédéric Bapst, Tony Licata
Lieu : domicile

## Ordre du jour
- validation du précédant PV
- avancements du rapport
- benchmarks établis
- tests unitaires ajoutés
- clean du code et ajout de commentaires

## Validation du précédant PV
le précédant PV a été validé

## Objectifs du projet
- utiliser le JIT durant les benchmarks? -> Non, nous sommes au courant des points qui ralentissent ProloGraal, désactiver le JIT pour effectuer de nouvelles mesures n'est pas pertinent
- écriture simplifiée de is/2: jeter un oeil sur la grammaire ANTLR de SimpleLanguage (prio des opérations, associativité)

## Rapport
- clarification du modèle de résumé du projet: les quelques caractéristiques du projet ont été clarifiés directement sur le document de résumé

## Tâches à réaliser
- ajouter les derniers tests unitaires
- terminer la documentation
