\documentclass[a4paper, 12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{titlesec}
\usepackage{listings}
\usepackage{color}
\usepackage{multicol}
\usepackage{booktabs}
\usepackage{pdfpages}
\usepackage{pdflscape}
\usepackage{lastpage}
\usepackage{subcaption}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\usepackage[a4paper, left=20mm, right=20mm]{geometry}

\setlength{\parindent}{0in}
\titlespacing\section{0pt}{6pt plus 2pt minus 2pt}{6pt plus 2pt minus 2pt}
\titlespacing\subsection{0pt}{6pt plus 2pt minus 2pt}{6pt plus 2pt minus 2pt}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=none,
  language=C,
  aboveskip=0mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=left,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3,
  morekeywords={assert, bool, uint32_t, uint64_t},
  deletekeywords={label}
}

\title{\vspace{-4,0cm}%
\includegraphics[width=\textwidth]{../logo.png}~
\\[2cm]
\Huge
\hrulefill\\
\vspace{1,0cm}
Prolog avec Truffle et GraalVM\\
Cahier des charges\\
\vspace{0,6cm}
\Large
\hrulefill\\\vspace{2,0cm}
Projet de semestre I-3\\
Automne 2019/2020
\vspace{2.5cm}}
\author{
  Martin Spoto \\
  martin.spoto@edu.hefr.ch\\
  \\
  Responsable :\\
  Frédéric Bapst\\
  Frederic.Bapst@hefr.ch
}
\begin{document}
\maketitle

\pagebreak
\tableofcontents
\pagebreak

\section{Contexte}
GraalVM est une machine virtuelle universelle basée sur la \textit{Java Virtual Machine} conçue pour permettre l'interprétation à haute performance de n'importe quel langage de programmation, pour autant qu'un interpréteur pour ce langage soit disponible.  Truffle est le nom du framework permettant l'implémentation en Java d'un tel interpréteur.

Des interpréteurs sont disponibles pour de nombreux langages, tels que JavaScript, Python, Ruby et même C et C++, mais il n'existe actuellement aucun interpréteur pour le langage de programmation logique Prolog.

Le but de ce projet est donc de réaliser cet interpréteur.
\section{Objectif du projet}
L'objectif du projet est d'obtenir un interpréteur Prolog proposant un certain nombre de fonctionnalités du langage. Truffle n'étant pas prévu de base pour supporter un langage de programmation logique, la décision de restreindre le langage à un ensemble de fonctionnalités jugées nécessaire pour fournir une preuve de concept a été prise.

Les fonctionnalités principales retenues dans le cadre du projet sont les suivantes :
\begin{itemize}
  \item Gestion de termes simples et composés
  \item Gestion de clauses comprenant des appels récursifs
  \item Gestion des variables
  \item Gestion de la notation simplifiée pour les listes
  \item Unification de termes, variables
  \item Gestion de la résolution par \textit{backtracking}
  \item Gestion des requêtes depuis la ligne de commande
\end{itemize}
Un certain nombre de fonctionnalités sont également retenues si l'implémentation s'avère plus rapide ou plus simple que prévue. Ces fonctionnalités sont listées sans ordre de priorité ci-dessous :
\begin{itemize}
  \item Gestion de calculs avec is/2
  \item Opérateur "!" (\textit{cut})
  \item \textit{assert} et \textit{retract}
  \item Gestion des exceptions
  \item Traceur de résolution
  \item Opérateurs mathématiques et logiques usuels (+ - * / = < >)
  \item Librairie standard, I/O
  \item Profiter de GraalVM/Truffle pour fournir une interface avec d'autres langages
  \item Profiter de GraalVM/Truffle pour fournir un débogueur
\end{itemize}
\section{Tâches}
Afin d'atteindre l'objectif du projet, il est nécessaire de découper la réalisation en plusieurs phases clés. Cette section décrit ces différentes phases et donne la liste du travail à réaliser pour chacune de ces phases. Un exemple de code Prolog devant fonctionner après chaque tâche est également fourni.
\subsection{Fonctions de base}
La première étape est d'obtenir un interpréteur capable de traiter des faits et requêtes simples, sans variable. Exemple :
\begin{lstlisting}
// programme
hello(world).
// interpreteur
?- hello(world).
yes
\end{lstlisting}
Cette première étape sert surtout à mettre en place l'environnement et la base du code pour le reste du projet.\\
Elle requiert les tâches suivantes :
\begin{itemize}
  \item Compréhension du fonctionnement de base de Truffle
  \item Compréhension de la syntaxe de Prolog
  \item Analyse des différentes implémentations existantes d'interpréteurs Prolog
  \item Compréhension du fonctionnement de base de ANTLR (requis pour générer le parser nécessaire à Truffle)
  \item Réalisation du parser
  \item Réalisation de l'interpréteur simple
\end{itemize}
\subsection{Variables}
La suite logique est de rajouter le support des variables, ce qui implique d'implémenter l'unification, ce qui implique à son tour d'implémenter le \textit{backtracking} pour réaliser l'arbre de preuve. Exemple :
\begin{lstlisting}
// programme
hello(world).
unify(A, A).
// interpreteur
?- hello(X).
  X = world
yes
?- unify(a, X).
  X = a
yes
\end{lstlisting}
Cette étape requiert les tâches suivantes :
\begin{itemize}
  \item Modification du parser pour les nouvelles notations
  \item Compréhension du fonctionnement de l'unification
  \item Modification de l'interpréteur pour les nouvelles fonctionnalités
\end{itemize}
\subsection{Clauses}
Une fois les variables ajoutées, le prochain pas est de supporter non plus de simples faits, mais des clauses complexes. Exemple :
\begin{lstlisting}
// programme
concat([], A, A).
concat([X | A], B, [X | R]) :-
  concat(A, B, R).
// interpreteur
?- concat([a,b], [c,d], X).

X = [a,b,c,d]

yes
\end{lstlisting}
Cette étape requiert les tâches suivantes :
\begin{itemize}
  \item Modification du parser pour les nouvelles notations
  \item Gestion de la récursion dans les clauses
\end{itemize}
\section{Planning}
Le planning se trouve en dernière page de ce document. Il est présenté sous la forme d'un diagramme de Gantt et reprend les différentes tâches du projet.
\includepdf[pages=-, landscape=true]{planning.pdf}
\end{document}
